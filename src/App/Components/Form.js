import React, { Component } from 'react'

import { FormStyled, Input, SubmitInput, Text } from './styles'

class Form extends Component {
  render() {
    return (
      <FormStyled>
        <Text>Fighter 1 HP</Text> <Input name="fighterHp" type="number" />
        <Text>Fighter 2 HP</Text>{' '}
        <Input name="improvedFighterHp" type="number" />
        <Text>rounds</Text> <Input name="rounds" type="number" />
        <SubmitInput type="submit" value="FIGHT" />
      </FormStyled>
    )
  }
}

export default Form
