import styled from 'styled-components'

export const ContainerWrapper = styled.main `
  height: 100vh;
  width: 100%;
  background-color: #1066d0c9;
  box-shadow: 0 0 8px 8px #1066d0c9;
  margin: -8 auto;
`

export const FormStyled = styled.form `
  display: flex;
  height: 70px;
  padding: 10px;
  margin: 0;
  align-items: center;
  justify-content: space-around;
`
export const Input = styled.input `
  text-align: center;
  width: 100px;
  box-shadow: 0 0 10px 10px #1066d0;
  height: 50px;
  font-size: 2em;
  margin-left: 10px;
  border: 2px solid #4477ff;
  border-radius: 5px;
  color: #0f0;
`
export const SubmitInput = styled.input `
  width: 100px;
  height: 50px;
  border: none;
  font-weight: 600;
  font-size: 20px;
  background-color: #ff0707;
`

export const Text = styled.label `
  width: 70px;
  font-size: 2em;
  color: #00ff00;
`
export const FieldStyle = styled.section `
  padding: 5%;
  width: 80%;
  margin: 0 auto;
  background-color: white;
  box-shadow: 0 0 200px #1066d0 inset;
`

export default {}