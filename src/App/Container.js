// @ts-nocheck
import React, { Component } from "react";

import { ContainerWrapper } from "./Components/styles";
import { Form, Field } from "./Components";

class Container extends Component {
  render() {
    return (
      <ContainerWrapper>
        <Form />
        <Field />
      </ContainerWrapper>
    );
  }
}

export default Container;
